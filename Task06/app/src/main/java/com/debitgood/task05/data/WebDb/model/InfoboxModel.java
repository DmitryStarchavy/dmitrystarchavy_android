package com.debitgood.task05.data.WebDb.model;

import com.google.gson.annotations.SerializedName;

public class InfoboxModel {

    @SerializedName("id")
    private String id;

    @SerializedName("area")
    private String area;

    @SerializedName("city_type")
    private String cityType;

    @SerializedName("city")
    private String city;

    @SerializedName("address_type")
    private String addressType;

    @SerializedName("address")
    private String address;

    @SerializedName("house")
    private String house;

    @SerializedName("install_place")
    private String installPlace;

    @SerializedName("location_name_desc")
    private String locationNameDesc;

    @SerializedName("work_time")
    private String workTime;

    @SerializedName("time_long")
    private String timeLong;

    @SerializedName("gps_x")
    private Double gpsX;

    @SerializedName("gps_y")
    private Double gpsY;

    @SerializedName("currency")
    private String currency;

    @SerializedName("inf_type")
    private String infType;

    @SerializedName("cash_in_exist")
    private Boolean cashInExist;

    @SerializedName("cash_in")
    private Boolean cashIn;

    @SerializedName("type_cash_in")
    private Boolean typeCashIn;

    @SerializedName("inf_printer")
    private Boolean inf_printer;

    @SerializedName("region_platej")
    private Boolean region_platej;

    @SerializedName("popolnenie_platej")
    private Boolean popolnenie_platej;

    @SerializedName("inf_status")
    private Boolean inf_status;

    public String getId() {
        return id;
    }

    public String getArea() {
        return area;
    }

    public String getCityType() {
        return cityType;
    }

    public String getCity() {
        return city;
    }

    public String getAddressType() {
        return addressType;
    }

    public String getAddress() {
        return address;
    }

    public String getHouse() {
        return house;
    }

    public String getInstallPlace() {
        return installPlace;
    }

    public String getLocationNameDesc() {
        return locationNameDesc;
    }

    public String getWorkTime() {
        return workTime;
    }

    public String getTimeLong() {
        return timeLong;
    }

    public Double getGpsX() {
        return gpsX;
    }

    public Double getGpsY() {
        return gpsY;
    }

    public String getCurrency() {
        return currency;
    }

    public String getInfType() {
        return infType;
    }

    public Boolean getCashInExist() {
        return cashInExist;
    }

    public Boolean getCashIn() {
        return cashIn;
    }

    public Boolean getTypeCashIn() {
        return typeCashIn;
    }

    public Boolean getInf_printer() {
        return inf_printer;
    }

    public Boolean getRegion_platej() {
        return region_platej;
    }

    public Boolean getPopolnenie_platej() {
        return popolnenie_platej;
    }

    public Boolean getInf_status() {
        return inf_status;
    }
}
