package com.debitgood.task05.domain.dto;


import java.util.Objects;

public class BelarusBank {
    private String typeApi;
    private String id;
    private String cityType;
    private String city;
    private String addressType;
    private String address;
    private String house;
    private String workTime;
    private Double gpsX;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BelarusBank)) return false;
        BelarusBank that = (BelarusBank) o;
        return getTypeApi().equals(that.getTypeApi()) &&
                getCityType().equals(that.getCityType()) &&
                getCity().equals(that.getCity()) &&
                getAddress().equals(that.getAddress()) &&
                getHouse().equals(that.getHouse()) &&
                Objects.equals(getWorkTime(), that.getWorkTime()) &&
                Objects.equals(getGpsX(), that.getGpsX()) &&
                Objects.equals(getGpsY(), that.getGpsY());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTypeApi(), getCityType(), getCity(), getAddress(), getHouse(), getWorkTime(), getGpsX(), getGpsY());
    }

    public BelarusBank(String typeApi, String id, String cityType, String city, String addressType,
                       String address, String house, String workTime, Double gpsX, Double gpsY) {
        this.typeApi = typeApi;
        this.id = id;
        this.cityType = cityType;
        this.city = city;
        this.addressType = addressType;
        this.address = address;
        this.house = house;
        this.workTime = workTime;
        this.gpsX = gpsX;
        this.gpsY = gpsY;
    }

    public String getTypeApi() {
        return typeApi;
    }

    public void setTypeApi(String typeApi) {
        this.typeApi = typeApi;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCityType() {
        return cityType;
    }

    public void setCityType(String cityType) {
        this.cityType = cityType;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getWorkTime() {
        return workTime;
    }

    public void setWorkTime(String workTime) {
        this.workTime = workTime;
    }

    public Double getGpsX() {
        return gpsX;
    }

    public void setGpsX(Double gpsX) {
        this.gpsX = gpsX;
    }

    public Double getGpsY() {
        return gpsY;
    }

    public void setGpsY(Double gpsY) {
        this.gpsY = gpsY;
    }

    private Double gpsY;


}
