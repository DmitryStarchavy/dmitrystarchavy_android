package com.debitgood.task05.data.WebDb.Services;

import com.debitgood.task05.data.WebDb.api.ApiServiceInfobox;
import com.debitgood.task05.data.di.RetrofitService;

import javax.inject.Inject;

import retrofit2.Retrofit;

public class ServicesInfobox {
    private Retrofit retrofit;

    @Inject
    public ServicesInfobox() {
        this.retrofit = RetrofitService
                .getInstance()
                .getRetrofit();
    }

    public ApiServiceInfobox getMainLIstApiService() {
        return retrofit.create(ApiServiceInfobox.class);
    }
}
