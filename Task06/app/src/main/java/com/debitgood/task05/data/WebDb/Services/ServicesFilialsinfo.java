package com.debitgood.task05.data.WebDb.Services;

import com.debitgood.task05.data.WebDb.api.ApiServiceFilialsInfo;
import com.debitgood.task05.data.di.RetrofitService;

import javax.inject.Inject;

import retrofit2.Retrofit;

public class ServicesFilialsinfo {
    private Retrofit retrofit;

    @Inject
    public ServicesFilialsinfo() {
        this.retrofit = RetrofitService
                .getInstance()
                .getRetrofit();
    }

    public ApiServiceFilialsInfo getMainLIstApiService() {
        return retrofit.create(ApiServiceFilialsInfo.class);
    }
}
