package com.debitgood.task05.data.repository;

import com.debitgood.task05.data.WebDb.Services.ServicesApi;
import com.debitgood.task05.data.WebDb.Services.ServicesFilialsinfo;
import com.debitgood.task05.data.WebDb.Services.ServicesInfobox;
import dagger.Component;

@Component
public interface RepositoryComponent {

    ApiListRepository apilistRepository();

    FilialsListRepository filialsListRepository();

    InfoboxListRepository infoboxListRepository();

    ServicesApi servicesApi();

    ServicesFilialsinfo servicesApi2();

    ServicesInfobox servicesApi3();

}
