package com.debitgood.task05;

import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;

import android.os.Bundle;

import com.debitgood.task05.domain.dto.BelarusBank;
import com.debitgood.task05.presentation.ViewModelBelarusbank;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    LiveData<List<BelarusBank>> liveData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        liveData = new ViewModelBelarusbank().getBank();
        liveData.observe(this, new Observer<List<BelarusBank>>() {
            @Override
            public void onChanged(@Nullable List<BelarusBank> belarusBank) {
                for (int i = 0; i < 10; i++) {
                    googleMap.addMarker(new MarkerOptions().position(new LatLng(belarusBank.get(i).getGpsX(), belarusBank.get(i).getGpsY())).title(
                            belarusBank.get(i).getTypeApi() + " " +
                                    belarusBank.get(i).getGpsX() + " " +
                                    belarusBank.get(i).getGpsY() + " " +
                                    belarusBank.get(i).getAddressType() + " " +
                                    belarusBank.get(i).getAddress() + " " +
                                    belarusBank.get(i).getHouse()));
                }
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(52.425163, 31.015039)));
                googleMap.animateCamera(CameraUpdateFactory.zoomTo(11));
            }
        });
    }
}
