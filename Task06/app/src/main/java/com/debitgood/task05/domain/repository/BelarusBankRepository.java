package com.debitgood.task05.domain.repository;

import com.debitgood.task05.domain.dto.BelarusBank;

import java.util.List;

import io.reactivex.Single;


public interface BelarusBankRepository {

    Single<List<BelarusBank>> getBank();
}
