package com.debitgood.task05.data.WebDb.api;

import com.debitgood.task05.data.WebDb.model.FilialsInfoModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiServiceFilialsInfo {

    @GET("filials_info")
    Single<List<FilialsInfoModel>> getApi(@Query("city") String city);
}
