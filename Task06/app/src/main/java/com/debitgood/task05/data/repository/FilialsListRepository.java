package com.debitgood.task05.data.repository;

import com.debitgood.task05.data.WebDb.Services.ServicesApi;
import com.debitgood.task05.data.WebDb.Services.ServicesFilialsinfo;
import com.debitgood.task05.data.mapper.FilialsInfoListMapper;
import com.debitgood.task05.domain.dto.BelarusBank;
import com.debitgood.task05.domain.repository.BelarusBankRepository;

import java.util.List;

import javax.inject.Inject;

import dagger.Module;
import io.reactivex.Single;


public class FilialsListRepository implements BelarusBankRepository {

    @Inject
     ServicesFilialsinfo servicesApi;

    @Inject
    public FilialsListRepository(ServicesFilialsinfo servicesApi) {
        this.servicesApi = servicesApi;
    }

    @Override
    public Single<List<BelarusBank>> getBank() {
        return servicesApi.getMainLIstApiService().getApi("Гомель")
                .map(FilialsInfoListMapper::map);
    }
}
