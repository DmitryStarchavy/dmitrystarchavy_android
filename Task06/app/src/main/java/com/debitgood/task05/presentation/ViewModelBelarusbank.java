package com.debitgood.task05.presentation;


import android.annotation.SuppressLint;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.debitgood.task05.data.repository.DaggerRepositoryComponent;
import com.debitgood.task05.data.repository.RepositoryComponent;
import com.debitgood.task05.domain.dto.BelarusBank;
import com.debitgood.task05.domain.repository.BelarusBankRepository;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import javax.inject.Inject;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ViewModelBelarusbank extends ViewModel {

    @Inject
    BelarusBankRepository apilistRepository;

    @Inject
    BelarusBankRepository filialsListRepository;

    @Inject
    BelarusBankRepository infoboxListRepository;


    private List<BelarusBank> belarusBank = new ArrayList<>();

    private MutableLiveData<List<BelarusBank>> bank;

    public LiveData<List<BelarusBank>> getBank() {
        if (bank == null) {
            bank = new MutableLiveData<List<BelarusBank>>();
            loadAtm();
        }
        return bank;
    }

    @SuppressLint("CheckResult")
    private void loadAtm() {
        RepositoryComponent repositoryComponent = DaggerRepositoryComponent.create();
        apilistRepository = repositoryComponent.apilistRepository();
        filialsListRepository = repositoryComponent.filialsListRepository();
        infoboxListRepository = repositoryComponent.infoboxListRepository();
        Single.zip(apilistRepository.getBank().subscribeOn(Schedulers.io()),
                filialsListRepository.getBank().subscribeOn(Schedulers.io()),
                infoboxListRepository.getBank().subscribeOn(Schedulers.io()), this::zipAll)
                .map(x -> new HashSet(x))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                            belarusBank.addAll(response);
                            Collections.sort(belarusBank, (o1, o2) -> (distance(o1) - distance(o2)));
                            bank.setValue(belarusBank);
                        });
    }

    private List<BelarusBank> zipAll(List<BelarusBank> s1, List<BelarusBank> s2, List<BelarusBank> s3) {
        List<BelarusBank> banki = new ArrayList<BelarusBank>();
        banki.addAll(s1);
        banki.addAll(s2);
        banki.addAll(s3);
        return banki;
    }

    private int distance(BelarusBank belarusBank) {
        final double x1 = 52.425163;
        final double y1 = 31.015039;
        double distance;
        distance = Math.sqrt(Math.pow((belarusBank.getGpsX() - x1), 2) +
                Math.pow((belarusBank.getGpsY() - y1), 2)) * 10000000;
        return (int) distance;
    }
}
