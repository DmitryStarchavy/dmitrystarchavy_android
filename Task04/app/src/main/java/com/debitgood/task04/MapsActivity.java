package com.debitgood.task04;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.util.Log;

import com.debitgood.task04.api.ApiService;
import com.debitgood.task04.model.AtmModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ApiService apiService;
    private List<AtmModel> atmModels = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        createApiService();
        loadAtm();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng gomel = new LatLng(52.4221751, 31.0145456);
        mMap.addMarker(new MarkerOptions().position(gomel).title("Гомель"));
        for (AtmModel model : atmModels) {
            mMap.addMarker(new MarkerOptions().position(new LatLng(model.getGpsX(), model.getGpsY())).title(model.getAddressType() + " " +
                    model.getAddress() + " " + model.getHouse()));
            Log.d("asd", model.getAddressType() + " " +
                    model.getAddress() + " " + model.getHouse());
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(gomel));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
    }

    private void getMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private void createApiService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://belarusbank.by/api/")
                .client(createOkkHttpClient())
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder()
                        .create()))
                .build();
        apiService = retrofit.create(ApiService.class);
    }

    private OkHttpClient createOkkHttpClient() {
        return new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
    }

    private void loadAtm() {
        apiService.getAtm("Гомель").enqueue(new Callback<List<AtmModel>>() {
            @Override
            public void onResponse(Call<List<AtmModel>> call, Response<List<AtmModel>> response) {
                if (response.body() != null) {
                    atmModels.addAll(response.body());
                    getMap();
                }
            }

            @Override
            public void onFailure(Call<List<AtmModel>> call, Throwable t) {
            }
        });
    }

}
