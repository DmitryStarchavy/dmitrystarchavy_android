package com.debitgood.task04.api;

import com.debitgood.task04.model.AtmModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {

    @GET("atm")
    Call<List<AtmModel>> getAtm(@Query("city") String city);
}
