package com.debitgood.task05.data.mapper;

import com.annimon.stream.Stream;
import com.debitgood.task05.data.WebDb.model.FilialsInfoModel;
import com.debitgood.task05.domain.dto.BelarusBank;

import java.util.List;

public class FilialsInfoListMapper {

    public static List<BelarusBank> map(List<FilialsInfoModel> model) {
        return Stream.range(0, model.size())
                .map(it -> new BelarusBank("Филлиал банка",
                        model.get(it).getId(),
                        model.get(it).getNameType(),
                        model.get(it).getName(),
                        model.get(it).getStreetType(),
                        model.get(it).getStreet(),
                        model.get(it).getHomeNumber(),
                        model.get(it).getInfoWorktime(),
                        model.get(it).getGpsX(),
                        model.get(it).getGpsY()))
                .toList();
    }
}
