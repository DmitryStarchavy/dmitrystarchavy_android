package com.debitgood.task05.data.WebDb.model;

import com.google.gson.annotations.SerializedName;

public class AtmModel {

    @SerializedName("id")
    private String id;

    @SerializedName("area")
    private String area;

    @SerializedName("city_type")
    private String cityType;

    @SerializedName("city")
    private String city;

    @SerializedName("address_type")
    private String addressType;

    @SerializedName("address")
    private String address;

    @SerializedName("house")
    private String house;

    @SerializedName("install_place")
    private String installPlace;

    @SerializedName("work_time")
    private String workTime;

    @SerializedName("gps_x")
    private Double gpsX;

    @SerializedName("gps_y")
    private Double gpsY;

    @SerializedName("install_place_full")
    private String installPlaceFull;

    @SerializedName("work_time_full")
    private String workTimeFull;

    @SerializedName("ATM_type")
    private String atmType;

    @SerializedName("ATM_error")
    private String atmError;

    @SerializedName("currency")
    private String currency;

    @SerializedName("cash_in")
    private String cashIn;

    @SerializedName("ATM_printer")
    private String atmPrinter;

    public String getId() {
        return id;
    }

    public String getArea() {
        return area;
    }

    public String getCityType() {
        return cityType;
    }

    public String getCity() {
        return city;
    }

    public String getAddressType() {
        return addressType;
    }

    public String getAddress() {
        return address;
    }

    public String getHouse() {
        return house;
    }

    public String getInstallPlace() {
        return installPlace;
    }

    public String getWorkTime() {
        return workTime;
    }

    public Double getGpsX() {
        return gpsX;
    }

    public Double getGpsY() {
        return gpsY;
    }

    public String getInstallPlaceFull() {
        return installPlaceFull;
    }

    public String getWorkTimeFull() {
        return workTimeFull;
    }

    public String getAtmType() {
        return atmType;
    }

    public String getAtmError() {
        return atmError;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCashIn() {
        return cashIn;
    }

    public String getAtmPrinter() {
        return atmPrinter;
    }
}
