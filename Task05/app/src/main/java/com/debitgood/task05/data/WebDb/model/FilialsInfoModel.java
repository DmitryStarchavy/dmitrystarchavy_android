package com.debitgood.task05.data.WebDb.model;

import com.google.gson.annotations.SerializedName;

public class FilialsInfoModel {

    @SerializedName("id")
    private String id;

    @SerializedName("filial_name")
    private String filialName;

    @SerializedName("filial_num")
    private String filialNum;

    @SerializedName("cbu_num")
    private String cbuNum;

    @SerializedName("otd_num")
    private String otdNum;

    @SerializedName("name_type")
    private String nameType;

    @SerializedName("name")
    private String name;

    @SerializedName("street_type")
    private String streetType;

    @SerializedName("street")
    private String street;

    @SerializedName("home_number")
    private String homeNumber;

    @SerializedName("name_type_prev")
    private String nameTypePrev;

    @SerializedName("name_prev")
    private String namePrev;

    @SerializedName("street_type_prev")
    private String streetTypePrev;

    @SerializedName("street_prev")
    private String streetPrev;

    @SerializedName("home_number_prev")
    private String homeNumberPrev;

    @SerializedName("info_text")
    private String infoText;

    @SerializedName("info_worktime")
    private String infoWorktime;

    @SerializedName("info_bank_bik")
    private String infoBankBik;

    @SerializedName("info_bank_unp")
    private String infoBankUnp;

    @SerializedName("GPS_X")
    private Double gpsX;

    @SerializedName("GPS_Y")
    private Double gpsY;

    @SerializedName("bel_number_schet")
    private String belNumberSchet;

    @SerializedName("foreign_number_schet")
    private String foreignNumberSchet;

    @SerializedName("phone_info")
    private String phoneIInfo;

    public String getId() {
        return id;
    }

    public String getFilialName() {
        return filialName;
    }

    public String getFilialNum() {
        return filialNum;
    }

    public String getCbuNum() {
        return cbuNum;
    }

    public String getOtdNum() {
        return otdNum;
    }

    public String getNameType() {
        return nameType;
    }

    public String getName() {
        return name;
    }

    public String getStreetType() {
        return streetType;
    }

    public String getStreet() {
        return street;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public String getNameTypePrev() {
        return nameTypePrev;
    }

    public String getNamePrev() {
        return namePrev;
    }

    public String getStreetTypePrev() {
        return streetTypePrev;
    }

    public String getStreetPrev() {
        return streetPrev;
    }

    public String getHomeNumberPrev() {
        return homeNumberPrev;
    }

    public String getInfoText() {
        return infoText;
    }

    public String getInfoWorktime() {
        return infoWorktime;
    }

    public String getInfoBankBik() {
        return infoBankBik;
    }

    public String getInfoBankUnp() {
        return infoBankUnp;
    }

    public Double getGpsX() {
        return gpsX;
    }

    public Double getGpsY() {
        return gpsY;
    }

    public String getBelNumberSchet() {
        return belNumberSchet;
    }

    public String getForeignNumberSchet() {
        return foreignNumberSchet;
    }

    public String getPhoneIInfo() {
        return phoneIInfo;
    }
}
