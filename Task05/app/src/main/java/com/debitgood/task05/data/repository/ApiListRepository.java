package com.debitgood.task05.data.repository;

import com.debitgood.task05.data.WebDb.Services.ServicesApi;
import com.debitgood.task05.data.mapper.ApiListMapper;
import com.debitgood.task05.domain.dto.BelarusBank;
import com.debitgood.task05.domain.repository.BelarusBankRepository;

import java.util.List;

import io.reactivex.Single;

public class ApiListRepository implements BelarusBankRepository {

    private ServicesApi servicesApi = new ServicesApi();

    @Override
    public Single<List<BelarusBank>> getBank() {
        return servicesApi.getMainLIstApiService().getApi("Гомель")
                .map(ApiListMapper::map);
    }

}
