package com.debitgood.task05.data.WebDb.api;

import com.debitgood.task05.data.WebDb.model.InfoboxModel;
import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiServiceInfobox {

    @GET("infobox")
    Single<List<InfoboxModel>> getApi(@Query("city") String city);
}
