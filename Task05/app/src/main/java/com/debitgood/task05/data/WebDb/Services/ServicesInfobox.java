package com.debitgood.task05.data.WebDb.Services;

import com.debitgood.task05.data.WebDb.api.ApiServiceInfobox;
import com.debitgood.task05.data.di.RetrofitService;

import retrofit2.Retrofit;

public class ServicesInfobox {
    private Retrofit retrofit;

    public ServicesInfobox() {
        this.retrofit = RetrofitService
                .getInstance()
                .getRetrofit();
    }

    public ApiServiceInfobox getMainLIstApiService() {
        return retrofit.create(ApiServiceInfobox.class);
    }
}
