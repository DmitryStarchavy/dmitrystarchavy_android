package com.debitgood.task05.data.WebDb.api;

import com.debitgood.task05.data.WebDb.model.AtmModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiServiceAtm {

    @GET("atm")
    Single<List<AtmModel>> getApi(@Query("city") String city);
}
