package com.debitgood.task05.data.mapper;

import com.annimon.stream.Stream;
import com.debitgood.task05.data.WebDb.model.AtmModel;
import com.debitgood.task05.domain.dto.BelarusBank;

import java.util.List;

public class ApiListMapper {

    public static List<BelarusBank> map(List<AtmModel> model) {
        return Stream.range(0, model.size())
                .map(it -> new BelarusBank("Банкомат",
                        model.get(it).getId(),
                        model.get(it).getCityType(),
                        model.get(it).getCity(),
                        model.get(it).getAddressType(),
                        model.get(it).getAddress(),
                        model.get(it).getHouse(),
                        model.get(it).getWorkTime(),
                        model.get(it).getGpsX(),
                        model.get(it).getGpsY()))
                .toList();
    }
}
