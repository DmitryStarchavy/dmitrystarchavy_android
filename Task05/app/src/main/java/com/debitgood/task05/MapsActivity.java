package com.debitgood.task05;

import androidx.fragment.app.FragmentActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;

import com.debitgood.task05.data.repository.ApiListRepository;
import com.debitgood.task05.data.repository.FilialsListRepository;
import com.debitgood.task05.data.repository.InfoboxListRepository;
import com.debitgood.task05.domain.dto.BelarusBank;
import com.debitgood.task05.domain.repository.BelarusBankRepository;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private BelarusBankRepository apilistRepository = new ApiListRepository();
    private BelarusBankRepository filialsListRepository = new FilialsListRepository();
    private BelarusBankRepository infoboxListRepository = new InfoboxListRepository();
    private GoogleMap mMap;
    private List<BelarusBank> belarusBank = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        loadAtm();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        for (int i = 0; i < 10; i++) {
            mMap.addMarker(new MarkerOptions().position(new LatLng(belarusBank.get(i).getGpsX(), belarusBank.get(i).getGpsY())).title(
                    belarusBank.get(i).getTypeApi() + " " +
                            belarusBank.get(i).getGpsX() + " " +
                            belarusBank.get(i).getGpsY() + " " +
                            belarusBank.get(i).getAddressType() + " " +
                            belarusBank.get(i).getAddress() + " " +
                            belarusBank.get(i).getHouse()));
        }
        mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(52.425163, 31.015039)));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
    }

    private void getMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    private int distance(BelarusBank belarusBank) {
        final double x1 = 52.425163;
        final double y1 = 31.015039;
        double distance;
        distance = Math.sqrt(Math.pow((belarusBank.getGpsX() - x1), 2) +
                Math.pow((belarusBank.getGpsY() - y1), 2)) * 10000000;
        return (int) distance;
    }

    @SuppressLint("CheckResult")
    private void loadAtm() {
        Single.zip(apilistRepository.getBank().subscribeOn(Schedulers.io()),
                filialsListRepository.getBank().subscribeOn(Schedulers.io()),
                infoboxListRepository.getBank().subscribeOn(Schedulers.io()), this::zipAll)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                            belarusBank.addAll(response);
                            Collections.sort(belarusBank, (o1, o2) -> (distance(o1) - distance(o2)));
                            getMap();
                        },
                        error -> {
                            Log.d("asd", "error " + error.getMessage());
                        });
    }

    private List<BelarusBank> zipAll(List<BelarusBank> s1, List<BelarusBank> s2, List<BelarusBank> s3) {
        List<BelarusBank> banki = new ArrayList<BelarusBank>();
        banki.addAll(s1);
        banki.addAll(s2);
        banki.addAll(s3);
        return banki;
    }

}
