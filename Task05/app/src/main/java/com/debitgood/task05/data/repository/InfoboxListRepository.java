package com.debitgood.task05.data.repository;

import com.debitgood.task05.data.WebDb.Services.ServicesInfobox;
import com.debitgood.task05.data.mapper.InfoboxListMapper;
import com.debitgood.task05.domain.dto.BelarusBank;
import com.debitgood.task05.domain.repository.BelarusBankRepository;

import java.util.List;

import io.reactivex.Single;

public class InfoboxListRepository implements BelarusBankRepository {

    private ServicesInfobox servicesApi = new ServicesInfobox();

    @Override
    public Single<List<BelarusBank>> getBank() {
        return servicesApi.getMainLIstApiService().getApi("Гомель")
                .map(InfoboxListMapper::map);
    }
}
