package com.debitgood.task05.data.mapper;

import com.annimon.stream.Stream;
import com.debitgood.task05.data.WebDb.model.InfoboxModel;
import com.debitgood.task05.domain.dto.BelarusBank;

import java.util.List;

public class InfoboxListMapper {

    public static List<BelarusBank> map(List<InfoboxModel> model) {
        return Stream.range(0, model.size())
                .map(it -> new BelarusBank("Инфокиоск",
                        model.get(it).getId(),
                        model.get(it).getCityType(),
                        model.get(it).getCity(),
                        model.get(it).getAddressType(),
                        model.get(it).getAddress(),
                        model.get(it).getHouse(),
                        model.get(it).getWorkTime(),
                        model.get(it).getGpsX(),
                        model.get(it).getGpsY()))
                .toList();
    }
}
