package com.debitgood.finaltask.data.WebDb.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Meta {
    @SerializedName("title")
    public String title;

    @SerializedName("image")
    public String image;

    @SerializedName("fields")
    public List<Field> fields;

    public String getTitle() {
        return title;
    }

    public String getImage() {
        return image;
    }

    public List<Field> getFields() {
        return fields;
    }

    @Override
    public String toString() {
        return "Meta{" +
                "title='" + title + '\'' +
                ", image='" + image + '\'' +
                ", fields=" + fields +
                '}';
    }
}
