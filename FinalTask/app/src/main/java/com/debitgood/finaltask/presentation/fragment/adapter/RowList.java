package com.debitgood.finaltask.presentation.fragment.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;

import androidx.recyclerview.widget.RecyclerView;

import com.debitgood.finaltask.data.WebDb.model.Values;
import com.debitgood.finaltask.presentation.ViewHolderFactory;

public class RowList implements RowType {

    private Context context;
    public String name;
    public Values value;

    @Override
    public int getItemViewType() {
        return RowType.LIST_ROW_TYPE;
    }

    public RowList(String name, Values value, Context context) {
        this.name = name;
        this.value = value;
        this.context = context;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder) {
        ViewHolderFactory.MetaViewHolderList textViewHolder = (ViewHolderFactory.MetaViewHolderList) viewHolder;
        textViewHolder.name.setText(name);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item,
                new String[]{value.getNone(), value.getV1(), value.getV2(), value.getV3()});
        textViewHolder.value.setAdapter(adapter);

    }
}