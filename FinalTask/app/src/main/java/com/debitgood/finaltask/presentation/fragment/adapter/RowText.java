package com.debitgood.finaltask.presentation.fragment.adapter;

import androidx.recyclerview.widget.RecyclerView;

import com.debitgood.finaltask.presentation.ViewHolderFactory;

public class RowText implements RowType {

    public String name;
    public String value;

    public RowText(String name) {
        this.name = name;
    }

    @Override
    public int getItemViewType() {
        return RowType.TEXT_ROW_TYPE;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder) {
        ViewHolderFactory.MetaViewHolderText textViewHolder = (ViewHolderFactory.MetaViewHolderText) viewHolder;
        textViewHolder.name.setText(name);
    }
}
