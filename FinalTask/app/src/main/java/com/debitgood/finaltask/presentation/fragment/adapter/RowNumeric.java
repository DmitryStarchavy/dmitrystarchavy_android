package com.debitgood.finaltask.presentation.fragment.adapter;

import androidx.recyclerview.widget.RecyclerView;

import com.debitgood.finaltask.presentation.ViewHolderFactory;

public class RowNumeric implements RowType {
    public String name;
    public String value;

    @Override
    public int getItemViewType() {
        return RowType.NUMERIC_ROW_TYPE;
    }

    public RowNumeric(String name) {
        this.name = name;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder) {
        ViewHolderFactory.MetaViewHolderNumeric numericViewHolder = (ViewHolderFactory.MetaViewHolderNumeric) viewHolder;
        numericViewHolder.name.setText(name);
    }
}

