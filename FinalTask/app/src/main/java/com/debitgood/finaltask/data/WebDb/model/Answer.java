package com.debitgood.finaltask.data.WebDb.model;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Answer {
    @SerializedName("result")
    public String result;


    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Answer)) return false;
        Answer answer = (Answer) o;
        return Objects.equals(getResult(), answer.getResult());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getResult());
    }

    @Override
    public String toString() {
        return "Answer{" +
                "result='" + result + '\'' +
                '}';
    }
}
