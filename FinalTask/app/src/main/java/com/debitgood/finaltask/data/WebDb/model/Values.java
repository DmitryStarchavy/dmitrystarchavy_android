package com.debitgood.finaltask.data.WebDb.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Values {
    @SerializedName("none")
    public String none;

    @SerializedName("v1")
    public String v1;

    @SerializedName("v2")
    public String v2;

    @SerializedName("v3")
    public String v3;

    public static String[] name = new String[]{"none", "v1", "v2", "v3"};

    public String getNone() {
        return none;
    }

    public String getV1() {
        return v1;
    }

    public String getV2() {
        return v2;
    }

    public String getV3() {
        return v3;
    }

    @Override
    public String toString() {
        return "Values{" +
                "none='" + none + '\'' +
                ", v1='" + v1 + '\'' +
                ", v2='" + v2 + '\'' +
                ", v3='" + v3 + '\'' +
                '}';
    }
}
