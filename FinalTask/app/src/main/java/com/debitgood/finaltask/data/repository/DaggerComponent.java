package com.debitgood.finaltask.data.repository;

import com.debitgood.finaltask.data.WebDb.Services.ServicesApi;

import dagger.Component;

@Component
public interface DaggerComponent {
    ApiListRepository metaRepository();

    ServicesApi servicesApi();
}
