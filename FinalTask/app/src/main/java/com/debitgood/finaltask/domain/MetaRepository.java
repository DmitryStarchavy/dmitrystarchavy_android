package com.debitgood.finaltask.domain;

import com.debitgood.finaltask.data.WebDb.model.Answer;
import com.debitgood.finaltask.data.WebDb.model.Meta;
import com.google.gson.JsonObject;

import io.reactivex.Single;
import retrofit2.http.Body;


public interface MetaRepository {

    Single<Meta> getMeta();

    Single<Answer> sendAnswer(@Body JsonObject json);

}
