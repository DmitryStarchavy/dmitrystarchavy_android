package com.debitgood.finaltask.data.WebDb.Services;


import com.debitgood.finaltask.data.WebDb.api.ApiServiceAtm;
import com.debitgood.finaltask.data.di.RetrofitService;

import javax.inject.Inject;

import retrofit2.Retrofit;

public class ServicesApi {

    private Retrofit retrofit;


    @Inject
    public ServicesApi() {
        this.retrofit = RetrofitService
                .getInstance()
                .getRetrofit();
    }

    public ApiServiceAtm getMainLIstApiService() {
        return retrofit.create(ApiServiceAtm.class);
    }
}
