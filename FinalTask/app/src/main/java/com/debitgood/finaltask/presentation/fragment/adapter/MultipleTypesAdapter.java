package com.debitgood.finaltask.presentation.fragment.adapter;

import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.debitgood.finaltask.presentation.ViewHolderFactory;

import java.util.List;

public class MultipleTypesAdapter extends RecyclerView.Adapter {

    public static List<RowType> dataSet;


    public MultipleTypesAdapter(List<RowType> dataSet) {
        this.dataSet = dataSet;
    }

    @Override
    public int getItemViewType(int position) {
        return dataSet.get(position).getItemViewType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return ViewHolderFactory.create(parent, viewType);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position) {
        dataSet.get(position).onBindViewHolder(holder);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
}
