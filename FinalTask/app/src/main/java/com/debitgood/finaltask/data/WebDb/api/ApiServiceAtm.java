package com.debitgood.finaltask.data.WebDb.api;

import com.debitgood.finaltask.data.WebDb.model.Answer;
import com.debitgood.finaltask.data.WebDb.model.Meta;
import com.google.gson.JsonObject;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;


public interface ApiServiceAtm {

    @GET("meta")
    Single<Meta> getData();

    @POST("data")
    Single<Answer> sendAnswer(@Body JsonObject json);
}
