package com.debitgood.finaltask.data.WebDb.model;

import com.google.gson.annotations.SerializedName;

public class Field {
    @SerializedName("title")
    public String title;

    @SerializedName("name")
    public String name;

    @SerializedName("type")
    public String type;

    @SerializedName("values")
    public Values values;

    public String getTitle() {
        return title;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public Values getValues() {
        return values;
    }

    @Override
    public String toString() {
        return "Field{" +
                "title='" + title + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", values=" + values +
                '}';
    }
}
