package com.debitgood.finaltask.data.repository;

import com.debitgood.finaltask.data.WebDb.Services.ServicesApi;
import com.debitgood.finaltask.data.WebDb.model.Answer;
import com.debitgood.finaltask.data.WebDb.model.Meta;
import com.debitgood.finaltask.domain.MetaRepository;
import com.google.gson.JsonObject;

import javax.inject.Inject;

import io.reactivex.Single;


public class ApiListRepository implements MetaRepository {

    @Inject
    ServicesApi servicesApi = DaggerDaggerComponent.create().servicesApi();

    @Inject
    public ApiListRepository(ServicesApi servicesApi) {
        this.servicesApi = servicesApi;
    }

    @Override
    public Single<Meta> getMeta() {
        return servicesApi.getMainLIstApiService().getData();
    }


    @Override
    public Single<Answer> sendAnswer(JsonObject json) {
        return servicesApi.getMainLIstApiService().sendAnswer(json);
    }


}
