package com.debitgood.finaltask.presentation.fragment;
import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import com.debitgood.finaltask.R;
import com.debitgood.finaltask.data.WebDb.model.Answer;
import com.debitgood.finaltask.data.WebDb.model.Field;
import com.debitgood.finaltask.data.WebDb.model.Meta;
import com.debitgood.finaltask.data.WebDb.model.Values;
import com.debitgood.finaltask.presentation.fragment.adapter.RowType;
import com.debitgood.finaltask.presentation.fragment.adapter.MultipleTypesAdapter;
import com.debitgood.finaltask.presentation.fragment.adapter.RowList;
import com.debitgood.finaltask.presentation.fragment.adapter.RowNumeric;
import com.debitgood.finaltask.presentation.fragment.adapter.RowText;
import com.debitgood.finaltask.presentation.viewModel.MetaViewModel;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;
import java.util.List;


public class MetaFragment extends Fragment {

    private MetaViewModel mViewModel;
    private RecyclerView recyclerView;
    private MultipleTypesAdapter metaAdapter;
    MutableLiveData<Meta> liveData;
    List<RowType> items = new ArrayList<>();
    LiveData<Answer> answer;
    ProgressBar progressBar;


    public static MetaFragment newInstance() {
        return new MetaFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.meta_fragment, container, false);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        progressBar = view.findViewById(R.id.progress);
        progressBar.setVisibility(ProgressBar.INVISIBLE);
        liveData = new MetaViewModel().getMeta(progressBar);
        liveData.observe(getViewLifecycleOwner(),
                meta ->
                {
                    ImageView imageView = view.findViewById(R.id.imageView);
                    Picasso.with(this.getContext()).load(meta.getImage()).into(imageView);
                    getActivity().setTitle(meta.getTitle());
                    for (Field field : meta.getFields()) {
                        switch (field.getType()) {
                            case "TEXT":
                                items.add(new RowText(field.getTitle()));
                                break;
                            case "NUMERIC":
                                items.add(new RowNumeric(field.getTitle()));
                                break;
                            case "LIST":
                                items.add(new RowList(field.getTitle(), field.getValues(), getContext()));
                                break;
                        }
                    }
                    metaAdapter = new MultipleTypesAdapter(items);
                    recyclerView = view.findViewById(R.id.recycler_for_fragment);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                    recyclerView.setAdapter(metaAdapter);
                }

        );

        Button buttonSend = view.findViewById(R.id.buttonSend);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JsonObject json = new JsonObject();
                ArrayList<Field> fields = new ArrayList<>();
                fields.addAll(liveData.getValue().getFields());
                for (int i = 0; i < fields.size(); i++) {
                    View viewGet = recyclerView.findViewHolderForAdapterPosition(i).itemView;
                    switch (fields.get(i).getType()) {
                        case "TEXT":
                        case "NUMERIC":
                            json.addProperty(fields.get(i).getName(),
                                    ((EditText) viewGet.findViewById(R.id.value)).getText().toString());
                            break;
                        case "LIST":
                            json.addProperty(Values.name[((Spinner) viewGet.findViewById(R.id.value)).getSelectedItemPosition()],
                                    ((Spinner) viewGet.findViewById(R.id.value)).getSelectedItem().toString());
                            break;
                    }
                }
                JsonObject jsonAll = new JsonObject();
                jsonAll.add("form", json);
                sendRequest(view, jsonAll);
            }
        });
    }

    private void sendRequest(View view, JsonObject json) {

        MetaViewModel modelAnswer = ViewModelProviders.of(this).get(MetaViewModel.class);
        answer = modelAnswer.getAnswer(json, progressBar);
        answer.observe(this, new Observer<Answer>() {
            @Override
            public void onChanged(Answer answer) {
                Log.d("asd", "response public void onChanged(" + answer.getResult());
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle("Ответ от сервера!")
                        .setMessage(answer.getResult())
                        .show();
            }
        });
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(MetaViewModel.class);


    }


}