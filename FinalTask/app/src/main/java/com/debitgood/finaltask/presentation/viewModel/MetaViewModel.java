package com.debitgood.finaltask.presentation.viewModel;

import android.annotation.SuppressLint;
import android.util.Log;
import android.widget.ProgressBar;

import androidx.appcompat.app.AlertDialog;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.debitgood.finaltask.data.WebDb.model.Answer;
import com.debitgood.finaltask.data.WebDb.model.Meta;
import com.debitgood.finaltask.data.repository.ApiListRepository;
import com.debitgood.finaltask.data.repository.DaggerDaggerComponent;
import com.google.gson.JsonObject;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class MetaViewModel extends ViewModel {

    @Inject
    ApiListRepository metaRepository = DaggerDaggerComponent.create().metaRepository();

    private MutableLiveData<Meta> meta;
    private MutableLiveData<Answer> answer;

    public MutableLiveData<Meta> getMeta(ProgressBar progressBar) {
        if (meta == null) {
            meta = new MutableLiveData<Meta>();
            loadMeta(progressBar);
        }
        return meta;
    }

    public MutableLiveData<Answer> getAnswer(JsonObject jsonObject, ProgressBar progressBar) {
        if (answer == null) {
            answer = new MutableLiveData<Answer>();
            sendAnswer(jsonObject, progressBar);
        }
        return answer;
    }

    @SuppressLint("CheckResult")
    private void sendAnswer(JsonObject jsonObject, ProgressBar progressBar) {
        progressBar.setVisibility(ProgressBar.VISIBLE);
        Single.wrap(metaRepository.sendAnswer(jsonObject))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        response -> {
                            answer.setValue(response);
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                            Log.d("asd", "response " + response);
                        },
                        error -> {
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(progressBar.getContext());
                            builder.setTitle("Ошибка ответа от сервера!")
                                    .setMessage(error.getMessage())
                                    .show();

                        });
    }


    @SuppressLint("CheckResult")
    private void loadMeta(ProgressBar progressBar) {
        progressBar.setVisibility(ProgressBar.VISIBLE);
        Single.wrap(metaRepository.getMeta())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(response -> {
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                            meta.setValue(response);
                        },
                        error -> {
                            progressBar.setVisibility(ProgressBar.INVISIBLE);
                            AlertDialog.Builder builder = new AlertDialog.Builder(progressBar.getContext());
                            builder.setTitle("Ошибка ответа от сервера!")
                                    .setMessage(error.getMessage())
                                    .show();
                        });
    }


}