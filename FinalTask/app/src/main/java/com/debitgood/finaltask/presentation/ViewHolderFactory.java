package com.debitgood.finaltask.presentation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.debitgood.finaltask.R;
import com.debitgood.finaltask.presentation.fragment.adapter.RowType;

public class ViewHolderFactory {

    public static class MetaViewHolderText extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView value;

        public MetaViewHolderText(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            value = itemView.findViewById(R.id.value);
        }
    }

    public static class MetaViewHolderNumeric extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView value;

        public MetaViewHolderNumeric(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            value = itemView.findViewById(R.id.value);
        }

    }

    public static class MetaViewHolderList extends RecyclerView.ViewHolder {

        public TextView name;
        public Spinner value;

        public MetaViewHolderList(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            value = itemView.findViewById(R.id.value);
        }
    }

    public static RecyclerView.ViewHolder create(ViewGroup parent, int viewType) {

        switch (viewType) {
            case RowType.TEXT_ROW_TYPE:
                View textTypeView = LayoutInflater.from(parent.getContext()).inflate(R.layout.meta_view_text, parent, false);
                return new ViewHolderFactory.MetaViewHolderText(textTypeView);

            case RowType.NUMERIC_ROW_TYPE:
                View numericTypeView = LayoutInflater.from(parent.getContext()).inflate(R.layout.meta_view_numeric, parent, false);
                return new ViewHolderFactory.MetaViewHolderNumeric(numericTypeView);

            case RowType.LIST_ROW_TYPE:
                View listTypeView = LayoutInflater.from(parent.getContext()).inflate(R.layout.meta_view_list, parent, false);
                return new ViewHolderFactory.MetaViewHolderList(listTypeView);

            default:
                return null;
        }
    }
}
