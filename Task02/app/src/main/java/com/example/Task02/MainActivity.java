package com.example.Task02;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.Task02.fragments.FragmentBig;
import com.example.Task02.fragments.FragmentList;
import com.example.Task02.interfaces.OnRecyclerViewItemClickListener;

public class MainActivity extends AppCompatActivity implements OnRecyclerViewItemClickListener {

    public static final String EXTRA_TEXT_TITLE = "Title";
    public static final String EXTRA_TEXT_DESCRIPTION = "Description";
    private Button replaceFragmentBtn;
    private FragmentManager fragmentManager;
    private Fragment listFragment;
    private Fragment bigFragment;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentManager = getSupportFragmentManager();
        findViews();
        setListeners();
    }

    private void findViews() {
        replaceFragmentBtn = findViewById(R.id.fragment_replace);
    }

    private void setListeners() {
        replaceFragmentBtn.setOnClickListener(v -> {
            if(fragmentManager.getBackStackEntryCount() == 0) {
                listFragment = FragmentList.newInstance(EXTRA_TEXT_TITLE,EXTRA_TEXT_DESCRIPTION);
                bundle = new Bundle();
                listFragment.setArguments(bundle);
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.fragment_container, listFragment, FragmentList.class.getSimpleName())
                        .addToBackStack(null)
                        .commit();
                fragmentManager.executePendingTransactions();
            }
        });
    }

    @Override
    public void onClick(String textViewTitle, String textViewDescription) {
        if(fragmentManager.getBackStackEntryCount() == 1) {
            bigFragment = FragmentBig.newInstance(textViewTitle,textViewDescription);
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment_container, bigFragment, FragmentBig.class.getSimpleName())
                    .addToBackStack(null)
                    .commit();
            fragmentManager.executePendingTransactions();
        }
    }
}
