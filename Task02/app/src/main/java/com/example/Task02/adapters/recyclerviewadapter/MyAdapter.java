package com.example.Task02.adapters.recyclerviewadapter;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.Task02.R;
import com.example.Task02.interfaces.OnRecyclerViewItemClickListener;

import java.util.ArrayList;
import java.util.List;

import static com.example.Task02.MainActivity.EXTRA_TEXT_DESCRIPTION;
import static com.example.Task02.MainActivity.EXTRA_TEXT_TITLE;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private OnRecyclerViewItemClickListener mClickListener;
    private List<String> adapterDatas = new ArrayList<>();

    public MyAdapter(List<String> myData) {
        adapterDatas.clear();
        adapterDatas.addAll(myData);
    }

    public void setmClickListener(OnRecyclerViewItemClickListener clickListener) {
        mClickListener = clickListener;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.fragment_view_holder, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.setText(adapterDatas.get(position));
    }

    @Override
    public int getItemCount() {
        return adapterDatas.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView textViewTitle;
        private TextView textViewDescription;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.text_view_title);
            textViewDescription = itemView.findViewById(R.id.text_view_description);
            itemView.setOnClickListener(v->{
                if (mClickListener != null) {
                    mClickListener.onClick(EXTRA_TEXT_TITLE.concat(String.valueOf(getLayoutPosition())),
                            EXTRA_TEXT_DESCRIPTION.concat(String.valueOf(getLayoutPosition())));
                }
            });
        }

        void setText(String text){
            textViewTitle.setText(EXTRA_TEXT_TITLE.concat(text));
            textViewDescription.setText(EXTRA_TEXT_DESCRIPTION.concat(text));
        }
    }
}
