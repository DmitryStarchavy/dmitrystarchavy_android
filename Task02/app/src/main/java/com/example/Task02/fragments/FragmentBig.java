package com.example.Task02.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import com.example.Task02.R;
import static com.example.Task02.MainActivity.EXTRA_TEXT_DESCRIPTION;
import static com.example.Task02.MainActivity.EXTRA_TEXT_TITLE;

public class FragmentBig  extends Fragment {
    private String argumentDescription;
    private String argumentTitle;

    public static FragmentBig newInstance(String param1,String param2) {
        Bundle args = new Bundle();
        args.putString(EXTRA_TEXT_TITLE, param1);
        args.putString(EXTRA_TEXT_DESCRIPTION, param2);
        FragmentBig fragment = new FragmentBig();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            argumentDescription = getArguments().getString(EXTRA_TEXT_TITLE);
            argumentTitle = getArguments().getString(EXTRA_TEXT_DESCRIPTION);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_view_holder_big, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        TextView textViewTitle = getActivity().findViewById(R.id.text_view_title);
        TextView textViewDescription = getActivity().findViewById(R.id.text_view_description);
        textViewTitle.setText(argumentTitle);
        textViewDescription.setText(argumentDescription);
    }

}

