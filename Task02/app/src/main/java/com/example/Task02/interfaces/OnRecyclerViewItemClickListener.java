package com.example.Task02.interfaces;

import android.widget.TextView;

public interface OnRecyclerViewItemClickListener {
    void onClick(String textViewTitle, String textViewDescription);
}
