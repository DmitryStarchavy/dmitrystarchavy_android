package com.example.Task02.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.Task02.R;
import com.example.Task02.adapters.recyclerviewadapter.MyAdapter;
import com.example.Task02.interfaces.OnRecyclerViewItemClickListener;

import java.util.ArrayList;
import java.util.List;

import static com.example.Task02.MainActivity.EXTRA_TEXT_DESCRIPTION;
import static com.example.Task02.MainActivity.EXTRA_TEXT_TITLE;

public class FragmentList extends Fragment {
    private String argumentDescription;
    private String argumentTitle;
    private MyAdapter adapter;
    private RecyclerView recyclerView;
    private RecyclerView fragmentText;

    public static FragmentList newInstance(String paramTitle,String paramDescription) {
        Bundle args = new Bundle();
        args.putString(EXTRA_TEXT_TITLE, paramTitle);
        args.putString(EXTRA_TEXT_DESCRIPTION, paramDescription);
        FragmentList fragment = new FragmentList();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recycler_for_fragment);
        adapter = new MyAdapter(createFakeData(1000));
        adapter.setmClickListener((OnRecyclerViewItemClickListener) getActivity());
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(adapter);
    }


    private List<String> createFakeData(int count) {
        int i = 0;
        List<String> fakeData = new ArrayList<>();
        while (i < count) {
            fakeData.add("" + i);
            i++;
        }
        return fakeData;
    }
}
