package com.debitgood.task03.room;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Contacts {
    @PrimaryKey(autoGenerate = true)
    public long id;

    public String phone;

    public String name;

    public String surname;

    public String mail;
}
