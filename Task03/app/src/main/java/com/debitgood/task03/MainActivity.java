package com.debitgood.task03;

import android.Manifest;
import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.debitgood.task03.room.AppDatabase;
import com.debitgood.task03.room.Contacts;
import com.debitgood.task03.room.ContactsDao;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MainActivity extends AppCompatActivity {
    private final static int REQUEST_CODE_PERMISSION_READ_CONTACTS = 1;
    private final static int PICK_CONTACT = 1;
    private final String CHANNEL_ID = "CHENNAL_MY";
    private Button buttonFindContact;
    private Button buttonShowContact;
    private Button buttonShowOfSp;
    private Button buttonShowOfNotification;
    private AppDatabase database = App.getInstance().getDatabase();
    private ContactsDao dao = database.contactsDao();
    private Contacts сontacts;
    List<Contacts> saveContacts;
    private TextView textName;
    private TextView textSurname;
    private TextView textPhone;
    private SharedPreferences sPref;
    private View rootlayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sPref = getPreferences(MODE_PRIVATE);
        setContentView(R.layout.activity_main);
        findViewsById();

    }

    private void findViewsById() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        rootlayout = findViewById(R.id.root_layout);
        buttonFindContact = findViewById(R.id.button_find_contact);
        buttonShowContact = findViewById(R.id.button_show_contact);
        buttonShowOfSp = findViewById(R.id.button_show_of_sp);
        buttonShowOfNotification = findViewById(R.id.button_show_of_Notification);
        textName = findViewById(R.id.text_name);
        textSurname = findViewById(R.id.text_surname);
        textPhone = findViewById(R.id.text_phone);
        setSupportActionBar(toolbar);
        setOnCLickListeners();
    }

    private void setOnCLickListeners() {

        buttonFindContact.setOnClickListener(v -> {
            if (!MainActivity.this.checkPermission(Manifest.permission.READ_CONTACTS)) {
                MainActivity.this.requestPermission(Manifest.permission.READ_CONTACTS,
                        REQUEST_CODE_PERMISSION_READ_CONTACTS);
            } else {
                findContact();
            }
        });

        buttonShowContact.setOnClickListener(v -> showDialog());

        buttonShowOfSp.setOnClickListener(v -> showSnackBar());

        buttonShowOfNotification.setOnClickListener(v -> showNotification());

    }

    private void showNotification() {
        String str = loadText("phone");
        if (str != null) {
            Runnable findByPhone = () -> {
                сontacts = dao.getByPhone(str);
            };
            Thread thread = new Thread(findByPhone);
            thread.start();
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (сontacts != null) {
                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                if (manager != null) {
                    createNotificationChannel(manager);
                    String message = "Имя - " + сontacts.name + ", Фамилия - " + сontacts.surname;
                    NotificationCompat.Style style = new NotificationCompat.BigTextStyle().bigText(message);
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID)
                            .setSmallIcon(R.drawable.ic_launcher_foreground)
                            .setContentTitle("Выбраный номер принадлежит:")
                            .setAutoCancel(true)
                            .setContentText(message)
                            .setStyle(style)
                            .setChannelId(CHANNEL_ID)
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);
                    manager.notify(321, builder.build());
                }
            }
        }
    }

    private void createNotificationChannel(NotificationManager manager) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, getString(R.string.app_name), NotificationManager.IMPORTANCE_DEFAULT);
            channel.setLightColor(Color.RED);
            channel.setLockscreenVisibility(NotificationCompat.VISIBILITY_PUBLIC);
            manager.createNotificationChannel(channel);
        }
    }

    private void showSnackBar() {
        String str = loadText("phone");
        if (!(str == null)) {
            Snackbar snackbar = Snackbar.make(rootlayout, str, BaseTransientBottomBar.LENGTH_INDEFINITE);
            snackbar.setAction("Ok", v -> snackbar.dismiss()).show();
        }
    }

    private void showDialog() {

        Runnable read = () -> {
            saveContacts = dao.getAll();
        };
        Thread thread = new Thread(read);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String[] list = new String[saveContacts.size()];
        for (int i = 0; i < saveContacts.size(); i++) {
            list[i] = saveContacts.get(i).phone;
        }
        getSupportFragmentManager()
                .beginTransaction()
                .add(MyDialogFragment.newInstance(list), null)
                .commit();
    }

    private void findContact() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, PICK_CONTACT);
    }

    private boolean checkPermission(String permission) {
        return ContextCompat.checkSelfPermission(this, permission) == PERMISSION_GRANTED;
    }

    private void requestPermission(String permission, int requestCode) {
        ActivityCompat.requestPermissions(this, new String[]{permission},
                requestCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_PERMISSION_READ_CONTACTS:
                if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED) {
                    findContact();
                } else {
                    Toast.makeText(this, "Доступ к контактам запрещён. " +
                            "Выбрать контакты не получиться.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        String contactID;
        switch (reqCode) {
            case (PICK_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor cursor = null;
                    cursor = getContentResolver().query(contactData, null,
                            null, null,null);
                    if (cursor.moveToFirst()) {
                        сontacts = new Contacts();
                        contactID = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
                        cursor = getContentResolver().query(ContactsContract.Data.CONTENT_URI, null,
                                ContactsContract.Data.MIMETYPE + " = ? AND " +
                                        ContactsContract.CommonDataKinds.StructuredName.CONTACT_ID + " = " + contactID,
                                new String[]{ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE}, null);
                        if (cursor.moveToNext()) {
                            сontacts.surname = cursor.getString(cursor.getColumnIndex(ContactsContract.
                                    CommonDataKinds.StructuredName.FAMILY_NAME));
                            сontacts.name = cursor.getString(cursor.getColumnIndex(ContactsContract.
                                    CommonDataKinds.StructuredName.GIVEN_NAME));
                        }
                        cursor = getContentResolver().query(
                                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " +
                                        contactID, null, null);
                        if (cursor.moveToNext()) {
                            сontacts.phone = cursor.getString(cursor.getColumnIndex(ContactsContract.
                                    CommonDataKinds.Phone.NUMBER));
                            cursor = getContentResolver().query(
                                    ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                                    ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = " +
                                            contactID, null, null);
                        }
                        if (cursor.moveToNext()) {
                            сontacts.mail = cursor.getString(cursor.getColumnIndex(ContactsContract.
                                    CommonDataKinds.Email.ADDRESS));
                        }
                        Runnable insert = () -> dao.insert(сontacts);
                        new Thread(insert).start();
                        Toast.makeText(getApplicationContext(), "Сохранение успешно",
                                Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    public void okClickedList(int which) {
        textName.setText(saveContacts.get(which).name);
        textSurname.setText(saveContacts.get(which).surname);
        textPhone.setText(saveContacts.get(which).phone);
        saveText("phone", saveContacts.get(which).phone);
    }

    private void saveText(String key, String text) {
        SharedPreferences.Editor editor = sPref.edit();
        editor.putString(key, text);
        editor.apply();
    }

    private String loadText(String key) {
        return sPref.getString(key, "");
    }
}
