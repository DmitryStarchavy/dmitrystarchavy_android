package com.debitgood.task03;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

public class MyDialogFragment extends AppCompatDialogFragment {

    public static MyDialogFragment newInstance(String[] list) {
        Bundle args = new Bundle();
        args.putStringArray("LIST", list);
        MyDialogFragment fragment = new MyDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setTitle("Выберете контакт")
                .setItems(getArguments().getStringArray("LIST"), (dialog, which) ->
                        ((MainActivity) getActivity()).okClickedList(which))
                .setCancelable(false);
        return builder.create();
    }
}
