package com.debitgood.task03.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;
import java.util.List;

@Dao
public interface ContactsDao {

    @Query("SELECT * FROM Contacts")
    List<Contacts> getAll();


    @Query("SELECT * FROM Contacts WHERE phone = :phone")
    Contacts getByPhone(String phone);


    @Query("SELECT * FROM Contacts WHERE id = :id")
    Contacts getById(long id);

    @Insert
    void insert(Contacts employee);

    @Update
    void update(Contacts employee);

    @Delete
    void delete(Contacts employee);

}